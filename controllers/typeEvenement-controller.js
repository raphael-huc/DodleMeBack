const TypeEvenement = require('../models/typeEvenement')

module.exports = {
    readAll(req,res) {
        TypeEvenement.find().then( (type) => {
            res.send(type);
        })
    },
    read(req, res) {
        const id = req.params.id;
        TypeEvenement.findById(id).then((type) => {
            res.send(type);
        });
    },

    create(req,res) {
        const body = req.body;
        const type = new TypeEvenement({libelle:body.libelle})
        type.save().then(() => {
            res.send(type);
        })
        console.log(body);
    },

    delete(req,res) {
        const {id} = req.body;
        TypeEvenement.findByIdAndRemove(id).then((type) => {
            res.send(type);
        })
    },

    update(req, res) {
        const body = req.body;
        const id = req.params.id;

        TypeEvenement.findByIdAndUpdate(id, {libelle:body.libelle}).then((type) => {
            // Renvoie la bonne valeur, si on renvoie l'objet type, on renvoie une valeur pas mis à jour
            TypeEvenement.findById(id).then((type) => {
                res.send(type);
            });
        })
    }
};
