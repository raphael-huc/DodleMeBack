const Invite = require('../models/invite');
const Evenement = require('../models/evenement');
const Utilisateur = require('../models/utilisateur');
const Notifier = require('../models/notifier');

module.exports = {
    readAll(req,res) {
        Invite.find().then( (invite) => {
            res.send(invite);
        })
    },
    read(req, res) {
        const id = req.params.id;
        Invite.findById(id).then((invite) => {
            res.send(invite);
        });
    },

    readByIDUser(req, res) {
        const utilisateur = req.params.user;
        Invite.find({utilisateur:utilisateur}).then((invite) => {
            res.send(invite);
        });
    },

    readByIDEvent(req, res) {
        const evenement = req.params.event;
        Invite.find({evenement:evenement}).then((events) => {
            res.send(events)
        })
    },

    readByIDUser(req, res) {
        const utilisateur = req.params.user;
        Invite.find({utilisateur:utilisateur}).then((invite) => {
            res.send(invite);
        });
    },

    readUserIDEvent(req, res) {
        const evenement = req.params.event;
        const users = new Array();
        Invite.find({evenement:evenement}).then((invite) => {
            var compt = 0;
            for(var i = 0; i < invite.length; i++) {
                Utilisateur.findById(invite[i].utilisateur).then((user) =>{
                    users.push(user);
                }).then( () => {
                    compt++;
                    if(compt == invite.length) {
                        res.send(users);
                    }
                })
            }
        })
    },

    create(req,res) {
        const body = req.body;
        Evenement.findById(body.evenement).then((event) => {
            Utilisateur.findById(body.utilisateur).then((user) => {
                const invite = new Invite({evenement:event, utilisateur:user})
                const notifier = new Notifier({evenement:event, utilisateur:user, type:'invite'});
                notifier.save();
                invite.save().then((invite) => {
                    res.send(invite);
                });
            });

        });
    },

   createByPseudos(req, res) {
       const body = req.body;
       let index = 0;
       let invites = [];
       Evenement.findById(body.evenement).then((event) => {
           for(var i = 0; i < body.pseudos.length; i++) {
               Utilisateur.findOne({pseudo: body.pseudos[i].pseudo}).then((user) => {
                   const invite = new Invite({evenement:event, utilisateur:user})
                   const notifier = new Notifier({evenement:event, utilisateur:user, type:'invite'});
                   notifier.save();
                   invite.save().then((invite) => {
                       invites.push(invite);
                       index++;
                       if(index == body.pseudos.length) {
                           res.send(invites);
                       }
                   });
               });
           }
       });

   },

    delete(req,res) {
        const {id} = req.body;
        Invite.findByIdAndRemove(id).then((invite) => {
            res.send(invite);
        })
    },
};
