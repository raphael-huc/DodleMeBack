const ObjectID = require('mongodb').ObjectID;

const Participe = require('../models/participe');
const Evenement = require('../models/evenement');
const Utilisateur = require('../models/utilisateur');
const Creneau = require('../models/creneau')

module.exports = {
    readAll(req,res) {
        Participe.find().then( (participe) => {
            res.send(participe);
        })
    },
    read(req, res) {
        const id = req.params.id;
        Participe.findById(id).then((participe) => {
            res.send(participe);
        });
    },

    readByIDUser(req, res) {
        const utilisateur = req.params.user;
        Participe.find({utilisateur:utilisateur}).then((participe) => {
            res.send(participe);
        });
    },

    readByIDEvent(req, res) {
        const evenement = req.params.event;
        Participe.find({evenement:evenement}).then((participe) => {
            res.send(participe)
        })
    },

    readByIDUserIDCreneau(req, res) {
        const user = req.params.user;
        const creneau= req.params.creneau;


        Participe.findOne({utilisateur:user, creneau:creneau}).then((participe) => {
            res.send(participe)
        })
    },

    readByIDUserEvent(req, res) {
        const body =  req.body;
        Participe.find({evenement:body.evenement, utilisateur:body.utilisateur}).then((participe) => {
            res.send(participe);
        })
    },

    readUserByIdEvent(req,res) {
        const evenement = req.params.event;
        const users = new Array();
        Participe.find({evenement:evenement}).then((participe) => {
            var compt = 0;
            for(var i = 0; i < participe.length; i++) {
                Utilisateur.findById(participe[i].utilisateur).then((user) =>{
                    users.push(user);
                }).then( () => {
                    compt++;
                    if(compt == participe.length) {
                        res.send(users);
                    }
                })
            }
        })
    },

    readEventByUserParticipe(req, res) {
        const user = req.params.user;
        const events = new Array();
        Participe.find({utilisateur:user}).then((ptcp) => {
            // Variable qui compte le nombre de fois qu'on entre dans le then, nécessaire puisque le then est fait après que la boucle for est terminé, étant un thread différent
            var compt = 0;
            for(var i = 0; i < ptcp.length; i++) {
                Evenement.findById(ObjectID(ptcp[i].evenement)).then((event) => {
                    events.push(event);
                }).then(() => {
                    compt++;
                    if(compt == ptcp.length) {
                        res.send(events);
                    }
                });
            }
        })
    },

    create(req,res) {
        const body = req.body;
        Evenement.findById(body.evenement).then((event) => {
            Utilisateur.findById(body.utilisateur).then((user) => {
                Creneau.findById(body.creneau).then((creneau) => {
                        const participe = new Participe({evenement:event, utilisateur:user, creneau:creneau, reponse:body.reponse})
                        participe.save().then((participe) => {
                            res.send(participe);
                        });
                });
            });

        });
    },

    delete(req,res) {
        const {id} = req.body;
        Participe.findByIdAndRemove(id).then((participe) => {
            res.send(participe);
        })
    },

    updateReponse(req, res) {
        const body = req.body;
        const id = req.params.id;

        Participe.findByIdAndUpdate(id, {reponse:body.reponse}).then((type) => {
            Participe.findById(id).then((participe) => {
                res.send(participe);
            });
        })
    }
};
