const Evenement = require('../models/evenement');
const Utilisateur = require('../models/utilisateur');
const Invite = require('../models/invite');
const Participe = require('../models/participe');
const Notifier = require('../models/notifier');
const Creneau = require('../models/creneau')

module.exports = {
    readAll(req,res) {
        Evenement.find().then( (events) => {
            res.send(events);
        })
    },
    read(req, res) {
        const id = req.params.id;
        Evenement.findById(id).then((event) => {
            res.send(event);
        });
    },

    readByIDUser(req, res) {
        const utilisateur = req.params.user;

        Evenement.find({createur:utilisateur}).then((events) => {
            res.send(events)
        })
    },

    readByIDUserEventCloture(req, res) {
        const utilisateur = req.params.user;

        Evenement.find({createur:utilisateur, dateCloture: { $ne: null}}).then((events) => {
            res.send(events)
        })
    },

    readByIDUserEventNotCloture(req, res) {
        const utilisateur = req.params.user;

        Evenement.find({createur:utilisateur}).where('dateCloture').equals(null).then((events) => {
            res.send(events)
        })
    },

    readByPseudo(req, res) {
        const pseudo = req.params.pseudo;
        Utilisateur.find({pseudo:pseudo}).then((user) => {
            Evenement.find({createur:user}).then((events) => {
                res.send(events)
            })
        })
    },

    readByInviteIDUser(req,res) {
        const id = req.params.user;
        Invite.find({utilisateur: id}).then((invites) => {
            let idEvents = [];
            for (var i = 0; i < invites.length; i++) {
                idEvents.push(invites[i].evenement);
            }
            Evenement.find({_id : {$in:idEvents}}).then((events) => {
                res.send(events)
            })
        })
    },

    readByInviteIDUserEventNotCloture(req,res) {
        const id = req.params.user;
        Invite.find({utilisateur: id}).then((invites) => {
            let idEvents = [];
            for (var i = 0; i < invites.length; i++) {
                idEvents.push(invites[i].evenement);
            }
            Evenement.find({_id : {$in:idEvents}}).where('dateCloture').equals(null).then((events) => {
                res.send(events)
            })
        })
    },

    readByInviteIDUserEventCloture(req,res) {
        const id = req.params.user;
        Invite.find({utilisateur: id}).then((invites) => {
            let idEvents = [];
            for (var i = 0; i < invites.length; i++) {
                idEvents.push(invites[i].evenement);
            }
            Evenement.find({_id : {$in:idEvents}, dateCloture: { $ne: null}}).then((events) => {
                res.send(events)
            })
        })
    },



    readNbParticipeByIDEvent(req,res) {
        const id = req.params.event;
        Participe.find({evenement: id}).then((invites) => {
            res.send(invites.length.toString());
        })
    },

    create(req,res) {
        const body = req.body;
        const evenement = new Evenement({titre: body.titre, description: body.description, typeEvenement: body.typeEvenement, createur: body.createur})
        evenement.save().then(() => {
            res.send(evenement);
        })

    },

    delete(req,res) {
        const id = req.body.id;
        Evenement.findOneAndRemove({_id: id}).then((event) => {
            Notifier.deleteMany({evenement: {$in : id}}).then((data) => {
                console.log("Delete Notifier")
            });
            Participe.deleteMany({evenement: {$in : id}}).then(() => {
                console.log("Delete Participe")
            });
            Creneau.deleteMany({evenement: {$in : id}}).then(() => {
                console.log("Delete Creneau")
            });
            Invite.deleteMany({evenement: {$in : id}}).then(() => {
                console.log("Delete Invite")
            });
            res.send(event);
        })
    },

    update(req, res) {
        const body = req.body;
        const id = req.params.id;
        Evenement.findByIdAndUpdate(id, {titre:body.titre, description:body.description, typeEvenement:body.typeEvenement}).then((type) => {
            Evenement.findById(id).then((event) => {
                res.send(event);
            });
        })
    },

    cloture(req, res) {
        const body = req.body;
        const id = req.params.id;
        Evenement.findByIdAndUpdate(id, {dateCloture:body.dateCloture}).then((type) => {
            Participe.distinct("utilisateur", {evenement: type._id}).then((participes) => {
                for(let i = 0; i < participes.length; i++) {
                    const notifier = new Notifier({evenement:type._id, utilisateur:participes[i], type:'cloture'});
                    notifier.save();
                }
            })
            Evenement.findById(id).then((event) => {
                res.send(event);
            });
        })
    },
};
