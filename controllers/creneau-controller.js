const Creneau = require('../models/creneau');
const Evenement = require('../models/evenement');
const Participe = require('../models/participe');

module.exports = {
    readAll(req,res) {
        Creneau.find().then( (creneau) => {
            res.send(creneau);
        })
    },
    read(req, res) {
        const id = req.params.id;
        Creneau.findById(id).then((creneau) => {
            res.send(creneau);
        });
    },

    readCreneauxAndNbParticipationByIDEvent(req, res) {
        const evenement = req.params.idEvent;
        let creneaux = new Array();
        let nbParticipeCreneaux = new Array();
        Creneau.find({evenement:evenement}).then((creneau) => {
            creneaux = creneau;
            let index = 0;
            for(let i = 0; i < creneaux.length; i++) {
                Participe.find({creneau:creneaux[i]._id,}).where('reponse').equals('true').then((participe) => {
                    nbParticipeCreneaux.push({creneau: creneaux[i], nbParticipe: participe.length});
                    if(index == creneaux.length - 1) {
                        res.send(nbParticipeCreneaux.sort(compare));
                    }
                    index++;
                })
            }
        });
    },


    readByIDEvent(req, res) {
        const evenement = req.params.id;
        Creneau.find({evenement:evenement}).then((creneau) => {
            res.send(creneau);
        });
    },

    create(req,res) {
        const body = req.body;
        Evenement.findById(body.evenement).then((event) => {
            const creneau = new Creneau({date:body.date,evenement:event})
            creneau.save().then(() => {
                res.send(creneau);
            })
        });
    },

    delete(req,res) {
        const body = req.body;
        Creneau.findByIdAndRemove(body.id).then((creneau) => {
            res.send(creneau);
        })
    },

    update(req, res) {
        const body = req.body;
        const id = req.params.id;

        TypeEvenement.findByIdAndUpdate(id, {date:body.date}).then(() => {
            TypeEvenement.findById(id).then((type) => {
                res.send(type);
            });
        })
    }
};

function compare(a, b) {
    const bandA = a.nbParticipe;
    const bandB = b.nbParticipe;

    let comparison = 0;
    if (bandA > bandB) {
        comparison = -1;
    } else if (bandA < bandB) {
        comparison = 1;
    }
    return comparison;
}
