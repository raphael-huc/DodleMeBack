const Utilisateur = require('../models/utilisateur');

module.exports = {
    // Tester api REST
    readAll(req,res) {
        Utilisateur.find().then( (event) => {
            res.send(event);
        })
    },

    read(req, res) {
        const id = req.params.id;
        Utilisateur.findById(id).then((event) => {
            res.send(event);
        });
    },

    readByIDPseudo(req, res) {
        const pseudo = req.params.pseudo;
        Utilisateur.findOne({pseudo:pseudo}).then((user) => {
            // Si l'utilisateur n'existe pas alors on le crée
            if(user === null) {
                const utilisateur = new Utilisateur({pseudo: pseudo, prenom: "", nom: "", mail: "", description: ""});
                utilisateur.save().then(() => {
                    res.send(utilisateur);
                })
            } else {
                res.send(user);
            }
        });
    },

    create(req,res) {
        const body = req.body;
        const utilisateur = new Utilisateur({pseudo: body.pseudo, prenom: body.prenom, nom: body.nom, mail: body.mail, description: body.description});
        utilisateur.save().then((user) => {
            res.send(user);
        });

    },

    delete(req,res) {
        const {id} = req.body;
        Utilisateur.findByIdAndRemove(id).then((user) => {
            res.send(user);
        })
    },

    update(req, res) {
        const body = req.body;
        const id = req.params.id;

        Utilisateur.findByIdAndUpdate(id, {prenom:body.prenom, nom:body.nom, mail:body.mail, description:body.description}).then(() => {
            Utilisateur.findById(id).then((user) => {
                res.send(user);
            });
        })
    },
};


