const Notifier = require('../models/notifier');
const Evenement = require('../models/evenement');
const Utilisateur = require('../models/utilisateur');

module.exports = {
    readAll(req,res) {
        Notifier.find().then( (notifier) => {
            res.send(notifier);
        })
    },
    read(req, res) {
        const id = req.params.id;
        Notifier.findById(id).then((notifier) => {
            res.send(notifier);
        })
    },

    readByIDUser(req, res) {
        const utilisateur = req.params.user;
        Notifier.find({utilisateur:utilisateur}).then((notifier) => {
            for(let i = 0; i < notifier.length; i++) {
                Notifier.findByIdAndRemove(notifier[i]._id).then((notifie) => {
                });
            }
            res.send(notifier);
        });
    },

    readByIDEvent(req, res) {
        const evenement = req.params.event;
        Notifier.find({evenement:evenement}).then((notifier) => {
            res.send(notifier);
        })
    },

    create(req,res) {
        const body = req.body;
        Evenement.findById(body.evenement).then((event) => {
            Utilisateur.findById(body.utilisateur).then((user) => {
                const notifier = new Notifier({evenement:event, utilisateur:user})
                notifier.save().then((notifier) => {
                    res.send(notifier);
                });
            });

        });
    },

    delete(req,res) {
        const {id} = req.body;
        Notifier.findByIdAndRemove(id).then((notifier) => {
            res.send(notifier);
        })
    },

};
