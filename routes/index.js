/* GET home page. */
//router.get('/', function(req, res, next) {
//  res.render('index', { title: 'Express' });
//});
EvenementController = require('../controllers/evenement-controller');
TypeEvenementController = require('../controllers/typeEvenement-controller');
CreneauController = require('../controllers/creneau-controller');
UtilisateurController = require('../controllers/utilisateur-controller');
InviteController = require('../controllers/invite-controller');
ParticipeController = require('../controllers/participe-controller');
NotifierController = require('../controllers/notifier-controller');

module.exports = (server) => {

  //Evenement
  server.get('/evenement', EvenementController.readAll);
  server.get('/evenementByUser/:user', EvenementController.readByIDUser);
  server.get('/evenementByUserEventCloture/:user', EvenementController.readByIDUserEventCloture);
  server.get('/evenementByUserEventNotCloture/:user', EvenementController.readByIDUserEventNotCloture);
  server.get('/evenementByPseudo/:pseudo', EvenementController.readByPseudo);
  server.get('/evenementByInviteUser/:user', EvenementController.readByInviteIDUser)
  server.get('/evenementByInviteUserEventCloture/:user', EvenementController.readByInviteIDUserEventCloture)
  server.get('/evenementByInviteUserEventNotCloture/:user', EvenementController.readByInviteIDUserEventNotCloture)
  server.get('/nbParticipeByEvent/:event', EvenementController.readNbParticipeByIDEvent)
  server.get('/evenement/:id', EvenementController.read);
  server.post('/evenement', EvenementController.create);
  server.delete('/evenement', EvenementController.delete);
  server.post('/evenement/:id', EvenementController.update);
  server.post('/evenementCloture/:id', EvenementController.cloture);

  //TypeEvenement
  server.get('/typeevenement', TypeEvenementController.readAll);
  server.get('/typeevenement/:id', TypeEvenementController.read);
  server.post('/typeevenement', TypeEvenementController.create);
  server.delete('/typeevenement', TypeEvenementController.delete);
  server.post('/typeevenement/:id', TypeEvenementController.update);


  //Creneau
  server.get('/creneau', CreneauController.readAll);
  server.get('/creneau/:id', CreneauController.read);
  server.get('/creneauByEvent/:id', CreneauController.readByIDEvent);
  server.get('/creneauxAndNbParticipeByEvent/:idEvent', CreneauController.readCreneauxAndNbParticipationByIDEvent);
  server.post('/creneau', CreneauController.create);
  server.delete('/creneau', CreneauController.delete);
  server.post('/creneau/:id', CreneauController.update);

  //Utilisateur
  server.get('/utilisateur', UtilisateurController.readAll);
  server.get('/utilisateur/:id', UtilisateurController.read);
  server.get('/utilisateurByPseudo/:pseudo', UtilisateurController.readByIDPseudo);
  server.post('/utilisateur', UtilisateurController.create);
  server.delete('/utilisateur', UtilisateurController.delete);
  server.post('/utilisateur/:id', UtilisateurController.update);

  //Invite
  server.get('/invite', InviteController.readAll);
  server.get('/invite/:id', InviteController.read);
  server.get('/inviteByUser/:user', InviteController.readByIDUser);
  server.get('/inviteByEvent/:event', InviteController.readByIDEvent);
  server.get('/invite/userByEvent/:event', InviteController.readUserIDEvent);
  server.post('/invite', InviteController.create);
  server.post('/inviteByPseudos', InviteController.createByPseudos);
  server.delete('/invite', InviteController.delete);

  //Participe
  server.get('/participe', ParticipeController.readAll);
  server.get('/participe/:id', ParticipeController.read);
  server.get('/participeByUser/:user', ParticipeController.readByIDUser);
  server.get('/participeByEvent/:event', ParticipeController.readByIDEvent);
  server.get('/participeByUserCreneau/:user/:creneau', ParticipeController.readByIDUserIDCreneau)
  server.get('/participe/eventByUser/:user', ParticipeController.readEventByUserParticipe);
  server.get('/participe/userByEvent/:event', ParticipeController.readUserByIdEvent);
  server.post('/participe', ParticipeController.create);
  server.delete('/participe', ParticipeController.delete);
  server.post('/participe/:id', ParticipeController.updateReponse);

  //Notifier
  server.get('/notifier', NotifierController.readAll);
  server.get('/notifier/:id', NotifierController.read);
  server.get('/notifierByUser/:user', NotifierController.readByIDUser);
  server.get('/notifierByEvent/:event', NotifierController.readByIDEvent);
  server.post('/notifier', NotifierController.create);
  server.delete('/notifier', NotifierController.delete);


  // méthode du prof avec un res.status voir ce qui est le mieux
  server.get('/api/test', function(req,res) {
    res.status(200).json("ça marche !");
  });
};
