const express = require('express');
const bodyparser = require('body-parser');
const routes = require('./routes/index')

const User = require('./models/utilisateur');
const TypeEvenement = require('./models/typeEvenement');
const Evenement = require('./models/evenement');
const Creneau = require('./models/creneau');

const mongoose = require('mongoose');
mongoose.Promise = global.Promise; // Supprime les warning

// adresse de la base mongoDB
const uri = "mongodb+srv://DodleMe_admin:dodleme@dodleme-3tvuz.mongodb.net/dodleme?retryWrites=true&w=majority";

const server = express();
server.use(bodyparser.json());

// formatage lorsqu'on utilise les get sur un navigateur
server.set('json spaces', 2);

// Permet d'autoriser les requêtes REST
server.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
});

routes(server);

server.listen(3000, function() {
    console.log("Serveur lancé sur le port 3000...");

    // Connexion à la base dodleme sur mongodb
    mongoose.connect(uri, {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false});
    mongoose.connection.once('open', () =>
    {
        console.log("connexion établie");
    }).on('error', (error) => {
        console.warn('Erreur durant la connexion', error)
    });


})

