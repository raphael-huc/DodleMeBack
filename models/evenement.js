const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EvenementSchema = new Schema({
    //CodeEvent:String,
    titre:String,
    description:String,
    typeEvenement: {
        type: Schema.Types.ObjectID,
        ref: 'typeEvenement'
    },
    dateCloture:Date,
    createur: {
        type: Schema.Types.ObjectID,
        ref: 'utilisateur'
    }
})

const Evenement = mongoose.model('evenement', EvenementSchema);
module.exports = Evenement;
