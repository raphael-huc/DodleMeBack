const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CreneauSchema = new Schema({
    //CodeCreneau:String,
    date: Date,
    evenement: {
        type: Schema.Types.ObjectID,
        ref: 'evenement'
    }
})

const Creneau = mongoose.model('creneau', CreneauSchema);

module.exports = Creneau;
