const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UtilisateurSchema = new Schema({
    pseudo:String,
    prenom:String,
    nom:String,
    mail:String,
    description:String
})
const Utilisateur = mongoose.model('utilisateur', UtilisateurSchema);

module.exports = Utilisateur;
