const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const NotifierSchema = new Schema({
    evenement: {
        type: Schema.Types.ObjectID,
        ref: 'evenement'
    },
    utilisateur: {
        type: Schema.Types.ObjectID,
        ref: 'utilisateur'
    },
    type: String
})

const Notifier = mongoose.model('notifier', NotifierSchema);

module.exports = Notifier;
