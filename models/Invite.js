const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const InviteSchema = new Schema({
    evenement: {
        type: Schema.Types.ObjectID,
        ref: 'evenement'
    },
    utilisateur: {
        type: Schema.Types.ObjectID,
        ref: 'utilisateur'
    },
})

const Invite = mongoose.model('invite', InviteSchema);

module.exports = Invite;
