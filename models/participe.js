const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ParticipeSchema = new Schema({
    evenement: {
        type: Schema.Types.ObjectID,
        ref: 'evenement'
    },
    utilisateur: {
        type: Schema.Types.ObjectID,
        ref: 'utilisateur'
    },
    creneau: {
        type: Schema.Types.ObjectID,
        ref: 'creneau'
    },
    reponse:Boolean
})

const Participe = mongoose.model('participe', ParticipeSchema);

module.exports = Participe;
