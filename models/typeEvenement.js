const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TypeEvenementSchema = new Schema({
    libelle:String
})

const TypeEvenement = mongoose.model('typeEvenement', TypeEvenementSchema);

module.exports = TypeEvenement;
